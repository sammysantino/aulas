package objeto;

import java.util.Scanner;

public class ExecutaProgramaBolo {

	public static void main(String[] args) {
		Bolo meusBolos[] = cadastrar();
		imprimir(meusBolos);
	}
	
	public static Bolo[] cadastrar() {
		Integer contador = 0;
		Bolo bolos[] = new Bolo[2];
		Bolo bolo = new Bolo();
		while (contador != 2) {
			Scanner ler = new Scanner(System.in);
			System.out.println("Digite o sabor: ");
			bolo.sabor = ler.nextLine();
			System.out.println("Digite o peso: ");
			bolo.peso = ler.nextDouble();
			bolos[contador] = bolo;
			System.out.println("Bolo adicionado com sucesso!");
			contador++;
		}
		
		return bolos;
	}
	
	public static void imprimir(Bolo bolinhas[]) {
		for (int i = 0; i < 2; i++) {
			System.out.println("Bolo sabor: " 
					+ bolinhas[i].sabor
					+ ", peso: "
					+ bolinhas[i].peso);
		}
	}

}
