package objeto;

//Construa uma classe que represente
//uma conta. Ela deve conter nome
//do	 titular	 (	String	),	 
//o	n�mero	 (	int	),	 
//a	 ag�ncia	 (	String	),	 
//o	 saldo	 (	double	)	 
//e	 uma	 data	 de
//abertura	 (	String	).	
//Al�m	 disso,	 ela	 deve	fazer	 
//as	 seguintes	 a��es:	 
//saca,	 para	 retirar	 um	 valor	 do saldo;	
//deposita,	para	adicionar	um	valor	ao	 saldo;	
//calculaRendimento,	para	devolver	o	 
//rendimento mensal	dessa	conta.

public class Conta {
	
	public String nomeTitular;
	public int numeroConta;
	public String agencia;
	public double saldo;
	public String dataAbertura;
	
	public void sacar(double valor) {
		saldo = saldo - valor;
		System.out.println("Saque de R$ " 
			+ valor 
			+ " efetuado.");
	}
	
	public void depositar(double valor) {
		saldo = saldo + valor;
		System.out.println("Dep�sito de R$ " 
			+ valor 
			+ " efetuado.");
	}
	
	public void calcularRendimento() {
		saldo = saldo + (saldo * 0.05);
		System.out.println("O rendimento mensal "
				+ "foi calculado.");
	}
	
	public void imprimirSaldo() {
		System.out.println("O saldo �: " + saldo);
	}
}
