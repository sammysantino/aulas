package empresa;

import java.util.Date;

public class Dependente {
	public String nome;
	public Date dataNascimento;
	public boolean participaPlano;
	public Double valorPlano;
	public boolean possuiCondicaoEspecial;
	public String descricaoCondicaoEspecial;
}
