package empresa;

import java.util.Date;

public class Startup {
	
	public static void main(String[] args) {
		Colaborador samara = new Colaborador();
		samara.cargo = "CEO";
		samara.salario = 15000.00;
		samara.cpf = "000.000.000-00";
		samara.dataNascimento = new Date();
		samara.nome = "S�mara";
		samara.dependente = new Dependente();
		
		System.out.println(samara.dependente.nome);
		
	}
	
}
