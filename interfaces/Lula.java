package interfaces;

//somente classes abstratas podem ter metodos abstratos
public abstract class Lula {
	
	
	
	//metodo concreto, ou seja, tem corpo {...}
	public void serEleito() {
		
	}
	
	
	//m�todo abstrato, sem corpo
	//quem herdar � obrigado a dar corpo
	//ou seja, implementar
	abstract void elegerHaddad();

}
