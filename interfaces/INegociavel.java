package interfaces;

public interface INegociavel {
	
	public boolean trocar();
	
	public void comprar(Double valorItem);
	
	//public void comprar(Double valorItem, Double valorDesconto);

}
