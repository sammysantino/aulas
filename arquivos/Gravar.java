package arquivos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Gravar {
	
	public static void main(String[] args) {
		File brau = 
				new File("C:\\Users\\FTI-Wictor\\teste_arquivos\\javaio.txt");
		
		FileWriter fw = null;
		try {
			fw = new FileWriter(brau);
			fw.write("Primeiro eu queria cumprimentar os internautas. -Oi Internautas"
					+ "\n oi outra linha!");
			brau = new File("C:\\Users\\FTI-Wictor\\teste_arquivos\\arquivo_thiaguera.txt");
			fw.close();
			fw = new FileWriter(brau);
			fw.write("outro arquivo legal \r outralinha \r ultima linha");
			fw.close();
			FileReader fr = new FileReader(brau);
			BufferedReader bf = new BufferedReader(fr);
			
			String s = bf.readLine();
			while (s != null) {
				System.out.println(s);
				s = bf.readLine();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

}
