package estatico;

public class TestaEstatico {

	public static void main(String[] args) {
		
		Aluno samara = new Aluno();
		samara.setNome("S�mara");
		Aluno renato = new Aluno();
		renato.setNome("Renato");
		Aluno thiago = new Aluno();
		thiago.setNome("Thiago");
		
		System.out.println(samara.getTurma());
		System.out.println(renato.getTurma());
		Aluno.setTurma("JAVA 2018");
		System.out.println(samara.getTurma());
		System.out.println(renato.getTurma());
		thiago.setTurma(".Net");
		samara.setTurma("Pizza!!!!");
		renato.setTurma("JAVA 2018");
	}

}
