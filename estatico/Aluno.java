package estatico;

public class Aluno {
	
	private String nome;
	private static String turma = "5B";
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		if (nome == null || nome.isEmpty()) {
			//erro
		} else {
			this.nome = nome;
		}
	}
	
	public static String getTurma() {
		return turma;
	}
	
	public static void setTurma(String turma) {
		Aluno.turma = turma;
	}

}
