package outro;

import objeto.*;

public class CaixaEletronico {

	public static void main(String[] args) {
		//objetos
		Conta jose = new Conta();
		jose.nomeTitular = "Jos�";
		
		Conta c = new Conta();
		c.nomeTitular = "Silvio Santos";
		
		Conta d = new Conta();
		d.nomeTitular = "Samara";
		
		d = c;
		c.nomeTitular = jose.nomeTitular;
		
		System.out.println(c.nomeTitular);
		System.out.println(d.nomeTitular);
		
		//primitivos
		int i = 5;
		int j = 10;
		
		j = i;
		i = 0;
		
		System.out.println(i);
		System.out.println(j);
	}
	
	public static void imprimir(Conta conta) {
		System.out.println("O saldo �: " + conta.saldo);
	}
	
	
	/*
	c.numeroConta = 123456;
	c.agencia = "ag 0987";
	c.dataAbertura = "20/09/2018";
	c.saldo = 10000.00;
	c.imprimirSaldo();
	c.depositar(3000.00);
	c.imprimirSaldo();
	c.calcularRendimento();
	c.imprimirSaldo();
	c.sacar(8000.00);
	c.imprimirSaldo();
	*/
	
}
