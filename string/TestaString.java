package string;

public class TestaString {
	
	public static void main(String args[]) {
		String nome = "S�mara Natascha Santino";
		//retorna o char na posicao passada como parametro
		System.out.println("charAt(1): " + nome.charAt(1));
		
		//separando num array de String com split
		String nomes[] = nome.split(" ");
		int ultimoIndex = nomes.length - 1;
		System.out.println("Ol� Sra " + nomes[ultimoIndex]);
		
		
		String outroNome = "S�MARA NATASCHA SANTINO";
		System.out.println("String upper case: " + outroNome);
		System.out.println("Para lower case: " + outroNome.toLowerCase());
		System.out.println("String camel case: " + nome);
		System.out.println("String upper case: " + nome.toUpperCase());
		
		//Exercicio
		//Receba uma string que contem o nome e endereco de uma pessoa
		//separado por ; 
		//Por sua vez o endereco vem separado por ,
		//Ex.: Anitta Alamanda Alameida Ferreira; Rua Oswaldo Cruz, 71, 
		//Bairro Lindo, Sao Paulo, SP
		//Faca um metodo para:
		//Imprimir somente o primeiro e o ultimo nome da pessoa
		//Os nomes do meio deve ser abreviados
		//Imprima em cada linha uma informacao diferente do endereco
		
		String texto = "Maria Flor Saralaias Zarara Zururu; "
				+ "Rua dos Amores, "
				+ "59, Bairro Centro, Sao Paulo, SP";
		
		String dados[] = texto.split(";");
		String nomes2[] = dados[0].split(" ");
		for (int i = 0; i < nomes2.length; i++) {
			if (i == 0 || i == (nomes2.length - 1)) {
				System.out.print(nomes2[i] + " ");
			} else {
				System.out.print(nomes2[i].charAt(0) + ". ");
			}
		}
		System.out.println();
		String enderecos[] = dados[1].split(",");
		for (int i = 0; i < enderecos.length; i++) {
			System.out.println(enderecos[i].trim());
		}
		
		//trabalhando com subtextos
		String codigoProduto = "BRPRgqwcrtb8ynw8rte513rq7er3b6tr7tn79wymgc802018";
		//nao esqueca: pega a posicao ate exclusive
		String pais = codigoProduto.substring(0, 2);
		String estado = codigoProduto.substring(2, 4);
		String ano = codigoProduto.substring(codigoProduto.length() - 4);
		System.out.println("O pa�s � " + pais);
		System.out.println("O estado � " + estado);
		System.out.println("O ano � " + ano);
		
		//remover o prefixo e sufixo
		String prefixo = codigoProduto.substring(0, 4);
		String sufixo = codigoProduto.substring(codigoProduto.length() - 4);
		//substituindo trecho do texto
		codigoProduto = codigoProduto.replace(prefixo, "");
		codigoProduto = codigoProduto.replace(sufixo, "");
		System.out.println(codigoProduto);
		
		//cor$peso$tamanho$origem
		//corpesotamanhoorigem = ref produto
		String codigo = "pink$180kg$2000cm$china";
		//indexOf() retorna o indica da primeira ocorrencia do(s) caractere(s)
		int sifrao = codigo.indexOf("$"); //posicao do primeiro $
		String cor = codigo.substring(0, sifrao); //pink
		codigo = codigo.substring(sifrao + 1); //180kg$2000cm$china
		sifrao = codigo.indexOf("$");
		String peso = codigo.substring(0, sifrao);//180kg
		codigo = codigo.substring(sifrao + 1); //2000cm$china
		sifrao = codigo.indexOf("$");
		String tamanho = codigo.substring(0, sifrao);//2000cm
		String origem = codigo.substring(sifrao + 1);//china
		System.out.println("Cor: " + cor);
		System.out.println("Peso: " + peso);
		System.out.println("Tamanho: " + tamanho);
		System.out.println("Origem: " + origem);
		System.out.println("Ref. Produto: " + cor + peso + tamanho + origem);
		
		//Fa�a um programa que receba um telefone e verifique se � v�lido 
		//(com 10 ou 11 posicoes), 
		//caso positivo formate este telefone (00) 0 0000-0000 ou (00) 0000-0000
		
		String telefone = "7934637463";
		if (telefone.length() < 10 || telefone.length() > 11) {
			System.out.println("Telefone inv�lido!");
		} else {
			String ddd = "(" + telefone.substring(0, 2) + ") ";
			telefone = telefone.substring(2);
			
			if (telefone.length() == 9) {
				String nonoDigito = telefone.substring(0, 1);
				telefone = telefone.substring(1);
				telefone = nonoDigito + " " + telefone;
			}
			int indiceUltimaParte = telefone.length() - 4;
			String primeiraParte = telefone.substring(0, indiceUltimaParte);
			String ultimaParte = telefone.substring(indiceUltimaParte);
			System.out.println(ddd + primeiraParte + "-" + ultimaParte);
		}
		
		//Substituir caracteres especiais
		String comEComercial = "amor&alegria&paz&amoedo&seNaoDer&bolsonaro2018";
		System.out.println(comEComercial.replace("&", " "));
		
		String gemeo1 = new String("Gemeo");
		String gemeo2 = new String("Gemeo");
		boolean ehIgual = (gemeo1 == gemeo2);
		System.out.println(ehIgual);
		
		Integer i = new Integer(1);
		Integer j = new Integer(1);
		System.out.println(i == j);
		
//		gemeo1 = "Joao";
//		gemeo2 = "Aparecido";
//		ehIgual = (gemeo1 == gemeo2);
//		System.out.println(ehIgual);
	}
}
