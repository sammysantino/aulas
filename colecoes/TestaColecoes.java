package colecoes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestaColecoes {
	
	public static void main(String[] args) {
		//List
		List<String> nomes = new ArrayList<>();
		//para cada (tipo) que eu vouchamar de (nomeVariavel)
		//dentro da lista (nomeDaLista) fa�a
		for (String nome : nomes) {
			//recupera indice de dado item
			int index = nomes.indexOf(nome);
			
			//inserir item no arrayList
			nomes.add(1, "Samara");
			nomes.add("Samara");
		}
		
		//
		List<String> alunosQueChegaramAntes = new ArrayList<>();
		alunosQueChegaramAntes.add("Lidia");
		alunosQueChegaramAntes.add("Marlu�");
		alunosQueChegaramAntes.add("Laura");
		
		List<String> alunosQueChegaramDepois = new ArrayList<>();
		alunosQueChegaramDepois.add("S�mara");
		alunosQueChegaramDepois.add("Mateus");
		alunosQueChegaramDepois.add("Jos�");
		
		//adiciona multiplos itens � lista
		alunosQueChegaramAntes.addAll(alunosQueChegaramDepois);
		
		//matriz para quem gosta
		List<List<String>> meuArray;
		ArrayList<ArrayList<String>> teste;
		
		for (int i = 0; i < nomes.size(); i++) {
			String nome = nomes.get(i);
		}
		
		String nomes2[] = new String[10];
		for (int i = 0; i < nomes2.length; i++) {
			String nome = nomes2[i];
		}
		
		//Ordenar
		Collections.sort(nomes);
		//nomes.sort(c);
		
		//Set nao contem elementos duplicados
		Set<String> times = new HashSet<>();
		times.add("Londrina");
		times.add("Santos");
		times.add("Corinthians");
		times.add("Corinthians");
		times.add("Clube Atl�tico Camb�");
		times.add("S�o Paulo");
		times.add("Palmeiras");
		times.add("Palmeiras");
		times.add("S�o Paulo");
		
		for (String t : times) {
			System.out.println(t);
		}
		
		//id, string
		Map<Integer, String> partidos = new HashMap<>();
		partidos.put(777, "PDSamara");
		partidos.put(51, "Patriota");
		partidos.put(13, "PT");
		partidos.put(777, "PDSamara");
		partidos.put(17, "PSL");
		partidos.put(19, "Podemos");
		partidos.put(30, "New");
		partidos.put(777, "PDSamara");
		partidos.put(18, "Rede");
		partidos.put(12, "PDT");
		partidos.put(45, "PSDB");
		partidos.put(777, "PDSamara");
		partidos.put(15, "MDB");
		partidos.put(777, "PDSamara");
		
		Set<Integer> chaves = partidos.keySet();
		for (Integer integer : chaves) {
			System.out.println(partidos.get(integer));
			System.out.println(integer);
		}
		
		Map<String, List<Integer>> notasPorAluno = new HashMap<>();
//		notasPorAluno.put("Jos�", Arrays.asList(10, 8, 3));
//		notasPorAluno.put("Lidia", Arrays.asList(3, 8, 10));
//		notasPorAluno.put("Cdf", Arrays.asList(10, 10, 10));
		notasPorAluno.put("Jos�", new ArrayList<>());
		notasPorAluno.put("Lidia", new ArrayList<>());
		notasPorAluno.put("Cdf", new ArrayList<>());
		
		//isso:
		Set<String> chaves2 = notasPorAluno.keySet();
		for (String chave : chaves2) {
			
		}
		
		//� exatamente igual a isso:
		for (String chave : notasPorAluno.keySet()) {
			if (chave.equals("Jos�")) {
				notasPorAluno.get("Jos�").add(10);//vai dar pau
			}
		}
		
		List<Integer> notas = new ArrayList<>();
		notas.add(1);
		notas.add(2);
		notas.add(3);
		
//		List<Integer> notas2 = new Integer[3];
//		notas2.add(1);
//		notas2.add(1);
//		notas2.add(1);
//		notas2.add(1);//pau!
	}

}
