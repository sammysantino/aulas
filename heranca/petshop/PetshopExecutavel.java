package heranca.petshop;

public class PetshopExecutavel {

	public static void main(String[] args) {
		Animal a = new Animal();
		a.locomoverSe();
		
		//Sempre chama�todos e atributos mais espec�ficos
		Ornitorrinco o = new Ornitorrinco();
		o.locomoverSe();
		//a = o;
		
		//polimorfismo: o obj � um ornitorrinco, 
		//porem esta sendo enxergado como se fosse um obj animal
		//mas � s� a forma de enxergar!!! 
		//ele continua sendo um ornitorrinco
		Animal refTipoAnimal = new Ornitorrinco(50);
		refTipoAnimal.locomoverSe();
		Ornitorrinco samara = (Ornitorrinco) refTipoAnimal;
		System.out.println(samara.indiceVeneno);
		refTipoAnimal.nome = "Ref Animal setou";
		System.out.println("samara imprimiu " + samara.nome);
		
		//metodo especifico do ornitorrinco nao pode ser 
		//acessado por referencia de superclasse
		o.getIndiceVeneno();
		//refTipoAnimal.get <- nao acha este metodo
		
		//esta linha da erro pq o animal nao � um ornitorrinco,
		//ou seja, animal NAO herda de ornitorrinco
		//Ornitorrinco o2 = (Ornitorrinco) new Animal();
		
		Cobra c = new Cobra();
		c.nome = "Celeste";
		
		Galinha g = new Galinha();
		g.nome = "Zaz�";
		
		Lagarto l = new Lagarto();
		l.nome = "Rango";
		
		vacinar(c);
		vacinar(g);
		vacinar(l);
		
		locomover(c);
		locomover(g);
		locomover(l);

	}
	
	public static void vacinar(Animal a) {
		System.out.println("o animal " 
				+ a.nome 
				+ " est� sendo vacinado");
	}
	
	public static void locomover(Animal a) {
		a.locomoverSe();
	}
	
}
