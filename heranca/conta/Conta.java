package heranca.conta;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Conta {
	
	private String titular;
	private ESexo sexo = ESexo.F;
	private Double saldo = new Double(0);
	public static final String VERSAO = "1212127-3547";
	
	public Conta() {
		
	}
	
	public void setSexo(String sexo) {
		ESexo sexoSelecionado = null;
		try {
			sexoSelecionado = ESexo.valueOf(sexo);
			
			try {
				String nomes[] = new String[1];
				System.out.println(nomes[1]);
			} catch (Exception e) {
				System.out.println("Tratei no try de dentro!");
			}
			
		} catch (IllegalArgumentException iae) {
			System.out.println("Deu erro na hora de achar o enum.");
		} catch (IndexOutOfBoundsException iae) {
			System.out.println("Deu erro na hora de acessao posicao do array.");
		} catch (Exception e) {
			System.out.println("Olha deu outro erro, mas nao me importa qual");
		}
		
		if (sexoSelecionado == null) {
			System.out.println("Sexo n�o localizado: " + sexo);
		} else {
			this.sexo = sexoSelecionado;
			System.out.println("Sexo localizado: " + sexo);
		}
	}
	
	public void setSexo2(String sexo) throws FileNotFoundException {
		try {
			if (sexo != null 
					&& !sexo.isEmpty()) {
				ESexo esexo = ESexo.valueOf(sexo); 
			} else {
				System.out.println("Informe um valor v�lido!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			PrintWriter writer = new PrintWriter("");
		}		
	}
	
	public void setSexo3(String sexo) throws RuntimeException {
		ESexo esexo = ESexo.valueOf(sexo); 
		this.sexo = esexo;
		String nomes[] = new String[1];
		System.out.println(nomes[1]);
		throw new RuntimeException();
	}
	
	public void setSexoAgoraVai(String sexo) throws SexoNaoEncontradoException {
		try {
			ESexo sexoSelecionado = ESexo.valueOf(sexo);
		} catch (IllegalArgumentException e) {
			throw new SexoNaoEncontradoException("Sexo n�o localizado:  " + sexo);
		}
	}
	
	public Conta(String titular, ESexo sexo) {
		this.titular = titular;
	}

	public String getTitular() {
		return titular;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void depositar(Double valor) {
		if (valor != null && valor > 0) {
			saldo = saldo + valor;
			System.out.println("Deposito na conta alterado");
		}
	}
	
	public void sacar(Double valor) {
		if (valor != null && valor <= saldo) { 
			saldo = saldo - valor;
			System.out.println("Saque da conta");
		}
	}
	
	public void sacar(String texto) {
		
	}

	@Override
	public boolean equals(Object obj) {
		Conta other = (Conta) obj;
		if (saldo == null) {
			if (other.saldo != null) {
				return false;
			}
		} else if (!saldo.equals(other.saldo)) {
			return false;
		}
		return true;
	}

}
