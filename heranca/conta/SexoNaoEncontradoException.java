package heranca.conta;

public class SexoNaoEncontradoException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public SexoNaoEncontradoException(String message) {
		super(message);
	}

}
