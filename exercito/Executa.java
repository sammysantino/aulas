package exercito;

public class Executa {
	
	public static void main(String args[]) {
		Soldado s1 = new Soldado();
		Soldado s2 = new Soldado("Renato", "Ferreira");
		Soldado s3 = new Soldado("S�mara", "Santino", "25/12/1990");
		System.out.println(
				s2.nome 
				+ " " 
				+ s2.sobrenome 
				+ " " 
				+ s2.dataNascimento);
		System.out.println(
				s3.nome 
				+ " " 
				+ s3.sobrenome 
				+ " " 
				+ s3.dataNascimento);
	}

}
