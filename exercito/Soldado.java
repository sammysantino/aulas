package exercito;

public class Soldado {
	
	public boolean homem;
	public String nome;
	public String sobrenome;
	public String dataNascimento;
	
	public Soldado() {
		
	}
	
	public Soldado(String primeiroNome,
			String segundoNome) {
		nome = primeiroNome;
		sobrenome = segundoNome;
	}
	
	public Soldado(String nome, 
			String sobrenome, 
			String dataNascimento) {
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.dataNascimento = dataNascimento;
	}

}
