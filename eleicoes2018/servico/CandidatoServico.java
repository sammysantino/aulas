package eleicoes2018.servico;

import eleicoes2018.Candidato;
import eleicoes2018.exception.ValidacaoException;

public class CandidatoServico {
	
	//gravar
	public static void gravar(Candidato candidato) {
		try {
			candidato.validarPreenchimento();
		} catch (ValidacaoException e) {
			System.out.println(e.getMessage());
		}
	}
	
	//recuperar
	public void ler() {
		
	}

}
