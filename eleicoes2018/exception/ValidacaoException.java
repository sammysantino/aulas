package eleicoes2018.exception;

public class ValidacaoException extends Exception {
	
	public ValidacaoException(String message) {
		super(message);
	}
}
