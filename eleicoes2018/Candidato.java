package eleicoes2018;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import eleicoes2018.enums.EPartidoPolitico;
import eleicoes2018.exception.ValidacaoException;
import heranca.pessoa.Pessoa;

public class Candidato extends Pessoa {
	private EPartidoPolitico partido;
	private Integer numeroEleitoral;
	private String dataNascimentoString;
	
	
	public void validarPreenchimento() throws ValidacaoException {
		StringBuilder validacao = new StringBuilder();
		
		if (!validarString(nome)) {
			validacao.append("Campo nome obrigat�rio.\n");
		} else if (nome.split(" ").length < 2) {
			validacao.append("O nome deve ser completo.\n");
		}
		
		if (!validarString(cpf)) {
			validacao.append("Campo CPF � obrigat�rio.\n");
		} else if (cpf.length() != 11) {
			validacao.append("O CPF informado � inv�lido.\n");
		}
		
		if (!validarString(dataNascimentoString)) {
			validacao.append("Campo Data Nascimento � obrigat�rio.\n");
		} else {
			String[] dataArray = dataNascimentoString.split("/");
			if (dataArray.length != 3) {
				validacao.append("Informe a Data Nascimento no formato dd/mm/aaaa.\n");
			} else {
				//conversao data
				DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				try {
					Date d = formatter.parse(dataNascimentoString);
					dataNascimento = d;
				} catch (ParseException e) {
					validacao.append("A Data Nascimento informada � inv�lida.\n");
				}
			}
		}
		
		if (validacao.length() > 0) {
			throw new ValidacaoException(validacao.toString());
		}
	}
	
	private boolean validarString(String s) {
		return s != null && !s.isEmpty();
	}

	public void setDataNascimentoString(String dataNascimentoString) {
		this.dataNascimentoString = dataNascimentoString;
	}
}
